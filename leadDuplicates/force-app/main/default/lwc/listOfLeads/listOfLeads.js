import { LightningElement, api, track, wire } from 'lwc';
import findDuplicates from '@salesforce/apex/DuplicateController.findDuplicates';

export default class ListOfLeads extends LightningElement {
    @api recordId;
    @track errorMsg;
               
    @wire(findDuplicates, { 
        recordId: '$recordId'
    })
    leadRecords

    get isDuplicateExist() {
        if(this.leadRecords.data != undefined && this.leadRecords.data.length != 0){
            return "height-block";
        } else {
            return "";
        }
    }
}