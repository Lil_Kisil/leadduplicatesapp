import { LightningElement, api } from 'lwc';

export default class LeadItem extends LightningElement {
    @api record;

    get getUrlPathToLead(){
        return '/lightning/r/Lead/' + this.record.Id + '/view';
    }
}