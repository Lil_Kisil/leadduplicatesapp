public with sharing class DuplicateController {
    @AuraEnabled(cacheable=true)
    public static List<Lead> findDuplicates (Id recordId) {
        List<Lead> property = [SELECT Email FROM Lead WHERE Id=:recordId WITH SECURITY_ENFORCED];
        String email = property[0].Email;

        List<Lead> duplicateListOfLeads = getLeadsByEmail(recordId, email);
        return duplicateListOfLeads;
    }

    private static List<Lead> getLeadsByEmail (Id recordId, String email) {
        return [SELECT Name, Company, CreatedDate, Email, LeadSource FROM Lead WHERE Id != :recordId AND Email = :email WITH SECURITY_ENFORCED ORDER BY CreatedDate DESC];
    }
}
