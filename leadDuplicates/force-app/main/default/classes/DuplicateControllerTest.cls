@isTest
public with sharing class DuplicateControllerTest {
    @testSetup
     static void setup() {
       Lead johnRecord = new Lead (
           FirstName = 'John',
           LastName = 'Terry',
           Company = 'KPI Telecom',
           LeadSource = 'Web',
           Email = 'john@cardinal.net'
       );
       insert johnRecord;

       Lead johnRecordDuplicate = new Lead (
           FirstName = 'Michael',
           LastName = 'Balack',
           Company = 'KPI Telecom',
           LeadSource = 'Web',
           Email = 'john@cardinal.net'
       );
       insert johnRecordDuplicate;

       Lead frankRecord = new Lead (
           FirstName = 'Frank',
           LastName = 'Lampard',
           Company = 'KPI Telecom',
           LeadSource = 'Web',
           Email = 'frank@cardinal.net'
       );
       insert frankRecord;
    }

    @isTest static void testFindDuplicates_FoundOutDuplicate() {
        Lead johnLead = [SELECT Id FROM Lead WHERE FirstName='John' AND LastName = 'Terry' WITH SECURITY_ENFORCED LIMIT 1];
        List<Lead> duplicatesList = DuplicateController.findDuplicates(johnLead.Id);
        System.assert(duplicatesList.size() == 1);
        System.assert(duplicatesList.get(0).Name == 'Michael Balack');
        System.assert(duplicatesList.get(0).Company == 'KPI Telecom');
        System.assert(duplicatesList.get(0).LeadSource == 'Web');
        System.assert(duplicatesList.get(0).Email == 'john@cardinal.net');
    }

    @isTest static void testFindDuplicates_NoMatches() {
        Lead frankLead = [SELECT Id FROM Lead WHERE FirstName='Frank' AND LastName = 'Lampard' WITH SECURITY_ENFORCED LIMIT 1];
        List<Lead> duplicatesList = DuplicateController.findDuplicates(frankLead.Id);
        System.assert(duplicatesList.size() == 0);
    }
}
